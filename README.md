# OpenML dataset: MiniBooNE

https://www.openml.org/d/44088

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "regression on numerical features" benchmark. Original description: 
 
This dataset is taken from the MiniBooNE experiment and is used to distinguish electron neutrinos (signal) from muon neutrinos (background).

This dataset is ordered. It first contains all signal observations, and then background observations.

B. Roe et al., 'Boosted Decision Trees, an Alternative to Artificial Neural Networks'
arXiv:physics/0408124, Nucl. Instrum. Meth. A543, 577 (2005).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44088) of an [OpenML dataset](https://www.openml.org/d/44088). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44088/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44088/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44088/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

